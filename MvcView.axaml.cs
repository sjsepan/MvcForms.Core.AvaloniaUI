#define DEBUG
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Ssepan.Utility.Core;
using Ssepan.Io.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.AvaloniaUI;
using MvcLibrary.Core;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Media.Imaging;

namespace MvcForms.Core.AvaloniaUI
{
    /// <summary>
    /// This is the View.
    /// Can base window on Window or its subclass ApplicationWindow
    /// </summary>
    public partial class MvcView :
        Window,//ApplicationWindow
        INotifyPropertyChanged
    {
        #region Declarations
        protected bool disposed;

        private static bool isHandlingClose;

		//cancellation hook
		// readonly Action cancelDelegate;
        protected MVCViewModel ViewModel;

        //Note: these names (or their equivalent) are supposedly already available now w/ version 11.0.x of AvaloniaUI
        #region declare menu
        // private MenuItem _menuFileNew;
        // private MenuItem _menuFileOpen;
        // private MenuItem _menuFileSave;
        // private MenuItem _menuFileSaveAs;
        // private MenuItem _menuFilePrint;
        // private MenuItem _menuFilePrintPreview;
        // private MenuItem _menuFileQuit;
        // private MenuItem _menuEditUndo;
        // private MenuItem _menuEditRedo;
        // private MenuItem _menuEditSelectAll;
        // private MenuItem _menuEditCut;
        // private MenuItem _menuEditCopy;
        // private MenuItem _menuEditPaste;
        // private MenuItem _menuEditPasteSpecial;
        // private MenuItem _menuEditDelete;
        // private MenuItem _menuEditFind;
        // private MenuItem _menuEditReplace;
        // private MenuItem _menuEditRefresh;
        // private MenuItem _menuEditPreferences;
        // private MenuItem _menuHelpContents;
        // private MenuItem _menuHelpCheckForUpdates;
        // private MenuItem _menuHelpAbout;
        #endregion declare menu

        // private Button _tbFileNew;
        // private Button _tbFileOpen;
        // private Button _tbFileSave;
        // private Button _tbFileSaveAs;
        // private Button _tbFilePrint;
        // private Button _tbEditUndo;
        // private Button _tbEditRedo;
        // private Button _tbEditCut;
        // private Button _tbEditCopy;
        // private Button _tbEditPaste;
        // private Button _tbEditDelete;
        // private Button _tbEditFind;
        // private Button _tbEditReplace;
        // private Button _tbEditRefresh;
        // private Button _tbEditPreferences;
        // private Button _tbHelpContents;

        #region declare status
        private TextBlock _statusMessage;
        private TextBlock _errorMessage;
        private ProgressBar _progressBar;
        private Image _pictureAction;
        private Image _pictureDirty;
        #endregion declare status

        #region declare fields
        //NOTE:unless these labels are going to be accessed/modified, their declaration is not necessary,
        // and will trigger a 'warning CS0414: The field 'MvcView._lblSomeInt' is assigned but its value is never used'
        // private TextBlock _lblSomeInt;
        // private TextBlock _lblSomeOtherInt;
        // private TextBlock _lblStillAnotherInt;
        // private TextBlock _lblSomeString;
        // private TextBlock _lblSomeOtherString;
        // private TextBlock _lblStillAnotherString;
        // private TextBlock _lblSomeBoolean;
        // private TextBlock _lblSomeOtherBoolean;
        // private TextBlock _lblStillAnotherBoolean;
        private TextBox _txtSomeInt;
        private TextBox _txtSomeOtherInt;
        private TextBox _txtStillAnotherInt;
        private TextBox _txtSomeString;
        private TextBox _txtSomeOtherString;
        private TextBox _txtStillAnotherString;
        private CheckBox _chkSomeBoolean;
        private CheckBox _chkSomeOtherBoolean;
        private CheckBox _chkStillAnotherBoolean;
        #endregion declare fields

        // private int _counter;
        #endregion Declarations

        #region Constructors
        public MvcView()
        {
            try
            {
                InitializeComponent();//auto-generated

                InitControls();

                #if DEBUG
                //Avalonia has an inbuilt DevTools window which is enabled by calling the attached AttachDevTools() method in a Window constructor. The default templates have this enabled when the program is compiled in DEBUG mode:
                //To open the DevTools, press F12, or pass a different Gesture to the this.AttachDevTools() method.
                // this.AttachDevTools();
                #endif

                //Not shown in titlebar, but visible in Alt-Tab
                //TODO:Set Icon "./Resources/App.png"

                _statusMessage.Text = "";
                _errorMessage.Text = "";

                ////(re)define default output delegate
                //ConsoleApplication.defaultOutputDelegate = ConsoleApplication.messageBoxWrapperOutputDelegate;

                //subscribe to view's notifications
                if (PropertyChanged != null)
                {
                    PropertyChanged += PropertyChangedEventHandlerDelegate;
                }

                InitViewModel();

                BindSizeAndLocation();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        private void InitControls()
        {
            //pre-Find frequently used controls

            #region define menu
            // _menuFileQuit = this.FindControl<MenuItem>("MenuFileQuit");

            #endregion define menu

            #region define status
            //status
            _statusMessage = this.FindControl<TextBlock>("StatusMessage");
            _errorMessage = this.FindControl<TextBlock>("ErrorMessage");
            _progressBar = this.FindControl<ProgressBar>("ProgressBar");
            _pictureAction = this.FindControl<Image>("ActionIcon");
            _pictureDirty = this.FindControl<Image>("DirtyIcon");
            #endregion define status

            #region define fields
            //pre-Find form fields
            _txtSomeInt = this.FindControl<TextBox>("txtSomeInt");
            _txtSomeOtherInt = this.FindControl<TextBox>("txtSomeOtherInt");
            _txtStillAnotherInt = this.FindControl<TextBox>("txtStillAnotherInt");
            _txtSomeString = this.FindControl<TextBox>("txtSomeString");
            _txtSomeOtherString = this.FindControl<TextBox>("txtSomeOtherString");
            _txtStillAnotherString = this.FindControl<TextBox>("txtStillAnotherString");
            _chkSomeBoolean = this.FindControl<CheckBox>("chkSomeBoolean");
            _chkSomeOtherBoolean = this.FindControl<CheckBox>("chkSomeOtherBoolean");
            _chkStillAnotherBoolean = this.FindControl<CheckBox>("chkStillAnotherBoolean");
            #endregion define fields

            #region bind control events
            //form events already wired up in .axaml file

            //menu events already wired up in .axaml file

            //toolbar events already wired up in .axaml file

            // misc controls should already be wired up in .axaml (but TextInput on TextBox is not)
            //https://github.com/AvaloniaUI/Avalonia/issues/3491
            //Note: the behavior is not as typically expected/desired: when the handler runs,
            //the TextBox .Text contains original value, not the new one that is visible,
            //and the event argument e.Text contains just the characters typed, not the new value.
            // _txtSomeInt.AddHandler(TextInputEvent, SomeInt_TextInput, RoutingStrategies.Tunnel);
            // _txtSomeOtherInt.AddHandler(TextInputEvent, SomeOtherInt_TextInput, RoutingStrategies.Tunnel);
            // _txtStillAnotherInt.AddHandler(TextInputEvent, StillAnotherInt_TextInput, RoutingStrategies.Tunnel);
            // _txtSomeString.AddHandler(TextInputEvent, SomeString_TextInput, RoutingStrategies.Tunnel);
            // _txtSomeOtherString.AddHandler(TextInputEvent, SomeOtherString_TextInput, RoutingStrategies.Tunnel);
            // _txtStillAnotherString.AddHandler(TextInputEvent, StillAnotherString_TextInput, RoutingStrategies.Tunnel);
            //https://github.com/AvaloniaUI/Avalonia/issues/418
            //Instead use an alternate observable subscription to obtain the new text value.
            //textBox.GetObservable(TextBox.TextProperty).Subscribe(text => { Console.WriteLine(text); });
            _txtSomeInt.GetObservable(TextBox.TextProperty).Subscribe(SomeInt_TextObserver);
            _txtSomeOtherInt.GetObservable(TextBox.TextProperty).Subscribe(SomeOtherInt_TextObserver);
            _txtStillAnotherInt.GetObservable(TextBox.TextProperty).Subscribe(StillAnotherInt_TextObserver);
            _txtSomeString.GetObservable(TextBox.TextProperty).Subscribe(SomeString_TextObserver);
            _txtSomeOtherString.GetObservable(TextBox.TextProperty).Subscribe(SomeOtherString_TextObserver);
            _txtStillAnotherString.GetObservable(TextBox.TextProperty).Subscribe(StillAnotherString_TextObserver);
            #endregion bind control events
        }
        #endregion Constructors

        #region Properties
        private string _ViewName = Program.APP_NAME;
        public string ViewName
        {
            get { return _ViewName; }
            set
            {
                _ViewName = value;
                OnPropertyChanged(nameof(ViewName));
            }
        }
        #endregion Properties

        #region INotifyPropertyChanged
        public new event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion INotifyPropertyChanged

        #region Handlers

        #region PropertyChangedEventHandlerDelegates
        /// <summary>
        /// Note: model property changes update UI manually.
        /// Note: handle settings property changes manually.
        /// Note: because settings properties are a subset of the model
        ///  (every settings property should be in the model,
        ///  but not every model property is persisted to settings)
        ///  it is decided that for now the settings handler will
        ///  invoke the model handler as well.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">PropertyChangedEventArgs</param>
        protected void PropertyChangedEventHandlerDelegate
        (
            object sender,
            PropertyChangedEventArgs e
        )
        {
            try
            {
                #region Model
                if (e.PropertyName == "IsChanged")
                {
                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", e.PropertyName));
                    ApplySettings();
                }
                //Status Bar
                else if (e.PropertyName == "ActionIconIsVisible")
                {
                    _pictureAction.IsVisible = ViewModel.ActionIconIsVisible;
                }
                else if (e.PropertyName == "ActionIconImage")
                {
                    _pictureAction.Source = (ViewModel?.ActionIconImage);
                }
                else if (e.PropertyName == "StatusMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    _statusMessage.Text = (ViewModel?.StatusMessage);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", StatusMessage));
                }
                else if (e.PropertyName == "ErrorMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    _errorMessage.Text = (ViewModel?.ErrorMessage);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "CustomMessage")
                {
                    //replace default action by setting control property
                    //StatusBarCustomMessage.Text = ViewModel.CustomMessage;
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "ErrorMessageToolTipText")
                {
                    _errorMessage.Tag = ViewModel.ErrorMessageToolTipText;
                }
                else if (e.PropertyName == "ProgressBarValue")
                {
                    _progressBar.Value = ViewModel.ProgressBarValue / 100;
                }
                else if (e.PropertyName == "ProgressBarMaximum")
                {
                    _progressBar.Maximum = ViewModel.ProgressBarMaximum;
                }
                else if (e.PropertyName == "ProgressBarMinimum")
                {
                    _progressBar.Minimum = ViewModel.ProgressBarMinimum;
                }
                else if (e.PropertyName == "ProgressBarStep")
                {
                    _progressBar.SmallChange = ViewModel.ProgressBarStep;
                }
                else if (e.PropertyName == "ProgressBarIsMarquee")
                {
                    _progressBar.IsIndeterminate = ViewModel.ProgressBarIsMarquee;
                }
                else if (e.PropertyName == "ProgressBarIsVisible")
                {
                    _progressBar.IsVisible = ViewModel.ProgressBarIsVisible;
                }
                else if (e.PropertyName == "DirtyIconIsVisible")
                {
                    _pictureDirty.IsVisible = ViewModel.DirtyIconIsVisible;
                }
                else if (e.PropertyName == "DirtyIconImage")
                {
                    _pictureDirty.Source = ViewModel.DirtyIconImage;
                }

                //Fields
                //use if properties cannot be databound
                else if (e.PropertyName == "SomeInt")
                {
                   _txtSomeInt.Text = ModelController<MVCModel>.Model.SomeInt.ToString();
                }
                else if (e.PropertyName == "SomeBoolean")
                {
                   _chkSomeBoolean.IsChecked = ModelController<MVCModel>.Model.SomeBoolean;
                }
                else if (e.PropertyName == "SomeString")
                {
                   _txtSomeString.Text = ModelController<MVCModel>.Model.SomeString;
                }
                else if (e.PropertyName == "StillAnotherInt")
                {
                   _txtStillAnotherInt.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt.ToString();
                }
                else if (e.PropertyName == "StillAnotherBoolean")
                {
                   _chkStillAnotherBoolean.IsChecked = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
                }
                else if (e.PropertyName == "StillAnotherString")
                {
                   _txtStillAnotherString.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString;
                }
                else if (e.PropertyName == "SomeOtherInt")
                {
                   _txtSomeOtherInt.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt.ToString();
                }
                else if (e.PropertyName == "SomeOtherBoolean")
                {
                   _chkSomeOtherBoolean.IsChecked = ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
                }
                else if (e.PropertyName == "SomeOtherString")
                {
                   _txtSomeOtherString.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherString;
                }
                //else if (e.PropertyName == "SomeComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("SomeComponent: {0},{1},{2}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt, ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean, ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
                //}
                //else if (e.PropertyName == "StillAnotherComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("StillAnotherComponent: {0},{1},{2}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
                //}
                else
                {
                    #if DEBUG_MODEL_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Model

                #region Settings
                if (e.PropertyName == "Dirty")
                {
                    //apply settings that don't have databindings
                    ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                }
                else
                {
                    #if DEBUG_SETTINGS_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Settings
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        #endregion PropertyChangedEventHandlerDelegates

        #region Form Events
        /// <summary>
        /// View_Load
        /// </summary>
        private void Window_Opened(object sender, EventArgs e)
        {
            try
            {
                ViewModel.StatusMessage = string.Format("{0} starting...", ViewName);

                ViewModel.StatusMessage = string.Format("{0} started.", ViewName);

                //_Run();
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                ViewModel.StatusMessage = string.Empty;

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// handles Window_Delete and (indirectly) FileQuit_Click
        /// </summary>
        private async void Window_Closing(object sender, /*RoutedEventArgs*//*Cancel*/WindowClosingEventArgs e)
        {
            try
            {
                //write close-checking around these semaphores
                if (isHandlingClose)
                {
                    //clean up data model here
                    ViewModel.StatusMessage = string.Format("{0} completing...", ViewName);
                    await DisposeSettings();
                    ViewModel.StatusMessage = string.Format("{0} completed.", ViewName);

                    ViewModel = null;
                }
                else
                {
                    //cancel because we haven't asked user yet
                    isHandlingClose = true;
                    e.Cancel = true;

                    //trigger FileQuitAction to ask user
                    if (await ViewModel.FileExit())
                    {
                        // fall out with Cancel = true

                        //reset flags for later
                        isHandlingClose = false;
                    }
                    else
                    {
                        Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Window_Closing" + ex.Message);
                //Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// TODO:handle ProcessCmdKey
        /// </summary>
        // [ GLib.ConnectBefore ] // need this to allow program to intercept the key first.
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
        //     bool returnValue = default(bool);
            try
            {

                // Implement the Escape / Cancel keystroke
                // if (keyData == Keys.Cancel || keyData == Keys.Escape)
                // {
                //     //if a long-running cancellable-action has registered
                //     //an escapable-event, trigger it
                //     InvokeActionCancel();

                //     // This keystroke was handled,
                //     //don't pass to the control with the focus
                //     returnValue = true;
                // }
                // else
                // {
                //     returnValue = base.ProcessCmdKey(ref msg, keyData);
                // }

            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        //     return returnValue;
        }

        #endregion Form Events

        #region Control Events

        private void ButtonColor_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.GetColor();
        }

        private void ButtonFont_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.GetFont();
        }

        // Action<string> SomeInt_TextObserver =
        // (string text) =>
        private void SomeInt_TextObserver(string text)
        {
			try
			{
				// Console.WriteLine("SomeInt_TextObserver:text"+text);
				if (ModelController<MVCModel>.Model != null)
				{
					ModelController<MVCModel>.Model.SomeInt =
						int.TryParse(text, out int result) ? result : 0;
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
			}
		}

        private void SomeOtherInt_TextObserver(string text)
        {
			try
			{
				// Console.WriteLine("SomeOtherInt_TextObserver:text"+text);
				if (ModelController<MVCModel>.Model != null)
				{
					ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt =
						int.TryParse(text, out int result) ? result : 0;
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
			}
		}

        private void StillAnotherInt_TextObserver(string text)
        {
			try
			{
				// Console.WriteLine("StillAnotherInt_TextObserver:text"+text);
				if (ModelController<MVCModel>.Model != null)
				{
					ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt =
						int.TryParse(text, out int result) ? result : 0;
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
			}
		}

        private void SomeString_TextObserver(string text)
        {
            try
            {
                // Console.WriteLine("SomeString_TextObserver:text"+text);
                if (ModelController<MVCModel>.Model != null)
                {
                    ModelController<MVCModel>.Model.SomeString = text;//_txtSomeString.Text;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        private void SomeOtherString_TextObserver(string text)
        {
            try
            {
                // Console.WriteLine("SomeOtherString_TextObserver:text"+text);
                if (ModelController<MVCModel>.Model != null)
                {
                    ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = text;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        private void StillAnotherString_TextObserver(string text)
        {
            try
            {
                // Console.WriteLine("StillAnotherString_TextObserver:text"+text);
                if (ModelController<MVCModel>.Model != null)
                {
                    ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = text;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        private void ChkSomeBoolean_Click(object sender, RoutedEventArgs e)
        {
            ModelController<MVCModel>.Model.SomeBoolean = _chkSomeBoolean.IsChecked.Value;
        }

        private void ChkSomeOtherBoolean_Click(object sender, RoutedEventArgs e)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = _chkSomeOtherBoolean.IsChecked.Value;
        }

        private void ChkStillAnotherBoolean_Click(object sender, RoutedEventArgs e)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = _chkStillAnotherBoolean.IsChecked.Value;
        }

        private void CmdRun_Click(object sender, RoutedEventArgs e)
        {
            //do something
            ViewModel.DoSomething();
        }

        #endregion Control Events

        #region Menu Events

        private async void MenuFileNew_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.FileNew();
        }

        private async void MenuFileOpen_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.FileOpen();
        }

        private async void MenuFileSave_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.FileSave();
        }

        private async void MenuFileSaveAs_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.FileSaveAs();
        }

        private void MenuFilePrint_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.FilePrint();
        }

        private void MenuFilePrintPreview_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.FilePrintPreview();
        }

        /// <summary>
        /// triggers Window_Delete which handles prompting user,
        /// and cancelling or quitting
        /// </summary>
        private void MenuFileQuit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuEditUndo_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditUndo();
        }

        private  void MenuEditRedo_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditRedo();
        }

        private  void MenuEditSelectAll_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditSelectAll();
        }

        private  void MenuEditCut_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditCut();
        }

        private  void MenuEditCopy_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditCopy();
        }

        private  void MenuEditPaste_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditPaste();
        }

        private  void MenuEditPasteSpecial_Click(object sender, RoutedEventArgs e)
        {
            //ViewModel.PasteSpecial();
        }

        private  void MenuEditDelete_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditDelete();
        }

        private  void MenuEditFind_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditFind();
        }

        private  void MenuEditReplace_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditReplace();
        }

        private  void MenuEditRefresh_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditRefresh();
        }

        private async void MenuEditPreferences_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.EditPreferences();
        }
        public void MenuWindowNewWindow_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.WindowNewWindow();
        }
        public void MenuWindowTile_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.WindowTile();
        }
        public void MenuWindowCascade_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.WindowCascade();
        }
        public void MenuWindowArrangeAll_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.WindowArrangeAll();
        }
        public void MenuWindowHide_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.WindowHide();
        }
        public void MenuWindowShow_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.WindowShow();
        }

        private  void MenuHelpContents_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.HelpContents();
        }
        private  void MenuHelpIndex_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.HelpIndex();
        }
        private  void MenuHelpOnlineHelp_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.HelpOnlineHelp();
        }
        private  void MenuHelpLicenseInformation_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.HelpLicenceInformation();
        }
        private async void MenuHelpCheckForUpdates_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.HelpCheckForUpdates();
        }
        private void MenuHelpAbout_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.HelpAbout<AssemblyInfo>();
        }
        #endregion Menu Events

        #region Toolbar Events
        private async  void ToolbarFileNew_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.FileNew();
        }
        private async void ToolbarFileOpen_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.FileOpen();
        }
        private async void ToolbarFileSave_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.FileSave();
        }
        private async void ToolbarFileSaveAs_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.FileSaveAs();
        }
        private void ToolbarFilePrint_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.FilePrint();
        }
        private  void ToolbarEditUndo_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditUndo();
        }
        private  void ToolbarEditRedo_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditRedo();
        }
        private  void ToolbarEditCut_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditCut();
        }
        private  void ToolbarEditCopy_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditCopy();
        }
        private  void ToolbarEditPaste_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditPaste();
        }
        private  void ToolbarEditDelete_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditDelete();
        }
        private  void ToolbarEditFind_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditFind();
        }
        private  void ToolbarEditReplace_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditReplace();
        }
        private  void ToolbarEditRefresh_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.EditRefresh();
        }
        private async void ToolbarEditPreferences_Click(object sender, RoutedEventArgs e)
        {
            await ViewModel.EditPreferences();
        }
        private  void ToolbarHelpContents_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.HelpContents();
        }
        #endregion Toolbar Events

        #endregion Handlers

        #region Methods
        #region FormAppBase
        protected async void InitViewModel()
        {
            FileDialogInfo<Window, MessageBox.MessageBoxResult> settingsFileDialogInfo = null;

            try
            {
                //tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
                ModelController<MVCModel>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                //tell controller how settings should notify view about persisted properties
                SettingsController<MVCSettings>.DefaultHandler = PropertyChangedEventHandlerDelegate;

				InitModelAndSettings();
				//settings used with file dialog interactions
				settingsFileDialogInfo =
					new FileDialogInfo<Window, MessageBox.MessageBoxResult>
					(
						parent: this,
						modal: true,
						title: null,
						response: MessageBox.MessageBoxResult.None,
						newFilename: SettingsController<MVCSettings>.FILE_NEW,
						filename: null,
						extension: SettingsBase.FileTypeExtension,
						description: SettingsBase.FileTypeDescription,
						typeName: SettingsBase.FileTypeName,
						additionalFilters:[
							// String.Format("{0} (*.{1})|{1}", SettingsBase.FileTypeDescription, SettingsBase.FileTypeExtension), //already defined above
							"JSON files (*.json)|json",
							"XML files (*.xml)|xml"//,
							// "All files (*.*)|*" //already defined in Ssepan.Application.Core.AvaloniaUI
						],
						multiselect: false,
						initialDirectory: default,
						forceDialog: false,
						forceNew: false,
						customInitialDirectory: Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator(),
						selectFolders: false
					)
					{
						//set dialog caption
						Title = Title
					};

				//class to handle standard behaviors
				ViewModelController<Bitmap, MVCViewModel>.New
                (
                    ViewName,
                    new MVCViewModel
                    (
                        PropertyChangedEventHandlerDelegate,
                        new Dictionary<string, Bitmap>()
                        { //TODO:ideally, should get these from library, but items added did not get generated into resource class.
                            { "New", new Bitmap("Resources/New.png") },
                            { "Open", new  Bitmap("Resources/Open.png") },
                            { "Save", new Bitmap("Resources/Save.png") },
                            { "SaveAs", new Bitmap("Resources/Save.png") },
                            { "Print", new Bitmap("Resources/Print.png") },
                            { "PrintPreview", new Bitmap("Resources/Print.png") },
                            { "Undo", new Bitmap("Resources/Undo.png") },
                            { "Redo", new Bitmap("Resources/Redo.png") },
                            { "Cut", new Bitmap("Resources/Cut.png") },
                            { "Copy", new Bitmap("Resources/Copy.png") },
                            { "Paste", new Bitmap("Resources/Paste.png") },
                            { "Delete", new Bitmap("Resources/Delete.png") },
                            { "Find", new Bitmap("Resources/Find.png") },
                            { "Replace", new Bitmap("Resources/Replace.png") },
                            { "Refresh", new Bitmap("Resources/Reload.png") },
                            { "Preferences", new Bitmap("Resources/Preferences.png") },
                            { "Properties", new Bitmap("Resources/Properties.png") },
                            { "Contents", new Bitmap("Resources/Contents.png") },
                            { "About", new Bitmap("Resources/About.png") }
                        },
                        settingsFileDialogInfo,
                        this//use ctor overload that takes view
                    )
                );

                //select a viewmodel by view name
                ViewModel = ViewModelController<Bitmap, MVCViewModel>.ViewModel[ViewName];

				// BindFormUi();

                //Init config parameters
                if (!LoadParameters())
                {
                    throw new Exception(string.Format("Unable to load config file parameter(s)."));
                }

                //DEBUG:filename coming in is being converted/passed as DOS 8.3 format equivalent
                //Load
                if ((SettingsController<MVCSettings>.FilePath == null) || SettingsController<MVCSettings>.Filename.StartsWith(SettingsController<MVCSettings>.FILE_NEW))
                {
                    //NEW
                    await ViewModel.FileNew();
                }
                else
                {
                    //OPEN
                    await ViewModel.FileOpen(false);
                }

#if debug
            //debug view
            menuEditProperties_Click(sender, e);
#endif

                //Display dirty state
                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        protected static void InitModelAndSettings()
        {
			try
			{
                //create Settings before first use by Model
                if (SettingsController<MVCSettings>.Settings == null)
                {
                    SettingsController<MVCSettings>.New();
                }
                //Model properties rely on Settings, so don't call Refresh before this is run.
                if (ModelController<MVCModel>.Model == null)
                {
                    ModelController<MVCModel>.New();
                }
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        protected async Task DisposeSettings()
        {
            MessageDialogInfo<Window, MessageBox.MessageBoxResult, object, MessageBox.MessageType, MessageBox.MessageBoxButtons> questionMessageDialogInfo = null;
            string errorMessage = null;

            //save user and application settings
            Properties.Settings.Default.Save();

            if (SettingsController<MVCSettings>.Settings.Dirty)
            {
                //prompt before saving
                questionMessageDialogInfo =
                    new MessageDialogInfo<Window, MessageBox.MessageBoxResult, object, MessageBox.MessageType, MessageBox.MessageBoxButtons>
                    (
                        null/*this*/,//Note: dialog gets error referencing parent window, which is closing
                        true,
                        "Save",
                        null,
                        MessageBox.MessageType.Info,//Question is not available
                        MessageBox.MessageBoxButtons.YesNo,
                        "Save changes?",
                        MessageBox.MessageBoxResult.None
                    );

                questionMessageDialogInfo = await Dialogs.ShowMessageDialog(questionMessageDialogInfo);
                if (!questionMessageDialogInfo.BoolResult)
                {
                    errorMessage = questionMessageDialogInfo.ErrorMessage;
                    throw new ApplicationException(errorMessage);
                }

                switch (questionMessageDialogInfo.Response)
                {
					case MessageBox.MessageBoxResult.Yes:
						//SAVE
						await ViewModel.FileSave();

						break;

					case MessageBox.MessageBoxResult.No:
						break;

					default:
						throw new InvalidEnumArgumentException();
				}
            }

            //unsubscribe from model notifications
            ModelController<MVCModel>.Model.PropertyChanged -= PropertyChangedEventHandlerDelegate;
        }

        protected static void Run()
        {
            //MessageBox.Show("running", "MVC", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }
        #endregion FormAppBase

        #region Utility
        // /// <summary>
        // /// Bind static Model controls to Model Controller
        // /// </summary>
        // private static void BindFormUi()
        // {
        //     try
        //     {
        //         //Form

        //         //Controls
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         throw;
        //     }
        // }

        // /// <summary>
        // /// Bind Model controls to Model Controller
        // /// Note: databinding not available in AvaloniaUI, but leave this in place
        // ///  in case I figure out a way to do this
        // /// </summary>
        // private void BindModelUi()
        // {
        //     try
        //     {
		// 		BindField(_txtSomeInt, ModelController<MVCModel>.Model, "SomeInt");
		// 		BindField(_txtSomeString, ModelController<MVCModel>.Model, "SomeString");
		// 		BindField(_chkSomeBoolean, ModelController<MVCModel>.Model, "SomeBoolean", "Checked");

		// 		BindField(_txtSomeOtherInt, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherInt");
		// 		BindField(_txtSomeOtherString, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherString");
		// 		BindField(_chkSomeOtherBoolean, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherBoolean", "Checked");

		// 		BindField(_txtStillAnotherInt, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherInt");
		// 		BindField(_txtStillAnotherString, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherString");
		// 		BindField(_chkStillAnotherBoolean, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherBoolean", "Checked");
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         throw;
        //     }
        // }

        // /// <summary>
        // /// TODO: databinding different in AvaloniaUI, but leave this in place
        // ///  in case I figure out how to use this
        // /// </summary>
        // private static void BindField<TControl, TModel>
        // (
        //     TControl fieldControl,
        //     TModel model,
        //     string modelPropertyName,
        //     string controlPropertyName = "Text",
        //     bool formattingEnabled = false,
        //     //DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged,
        //     bool reBind = true
        // )
        //     where TControl : Control
        // {
        //     try
        //     {
        //         //TODO: .RemoveSignalHandler ?
        //         //fieldControl.DataBindings.Clear();
        //         if (reBind)
        //         {
        //             //TODO:.AddSignalHandler ?
        //             //fieldControl.DataBindings.Add(controlPropertyName, model, modelPropertyName, formattingEnabled, dataSourceUpdateMode);
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Apply Settings to viewer.
        /// </summary>
        private void  ApplySettings()
        {
            try
            {
                // _ValueChangedProgrammatically = true;

                //apply settings that have databindings
                // BindModelUi();

                //apply settings that shouldn't use databindings

                //apply settings that can't use databindings
                Title = System.IO.Path.GetFileName(SettingsController<MVCSettings>.Filename) + " - " + ViewName;

                //apply settings that don't have databindings
                ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                //update remaining non-bound controls: textboxes, checkboxes
                //Note: ModelController<MVCModel>.Model.Refresh() will cause SO here
                ModelController<MVCModel>.Model.Refresh("SomeInt");
                ModelController<MVCModel>.Model.Refresh("SomeBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeString");
                ModelController<MVCModel>.Model.Refresh("SomeOtherInt");
                ModelController<MVCModel>.Model.Refresh("SomeOtherBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeOtherString");
                ModelController<MVCModel>.Model.Refresh("StillAnotherInt");
                ModelController<MVCModel>.Model.Refresh("StillAnotherBoolean");
                ModelController<MVCModel>.Model.Refresh("StillAnotherString");

                // _ValueChangedProgrammatically = false;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        // /// <summary>
        // /// Set function button and menu to enable value, and cancel button to opposite.
        // /// For now, do only disabling here and leave enabling based on biz logic
        // ///  to be triggered by refresh?
        // /// </summary>
        // /// <param name="functionButton">Button</param>
        // /// <param name="functionMenu">Button</param>
        // /// <param name="functionMenu">MenuItem</param>
        // /// <param name="cancelButton">Button</param>
        // /// <param name="enable">bool</param>
        // private void SetFunctionControlsEnable
        // (
        //     Button functionButton,
        //     Button functionToolbarButton,
        //     MenuItem functionMenu,
        //     Button cancelButton,
        //     bool enable
        // )
        // {
        //     try
        //     {
        //         //stand-alone button
        //         if (functionButton != null)
        //         {
        //             functionButton.IsEnabled = enable;
        //         }

        //         //toolbar button
        //         if (functionToolbarButton != null)
        //         {
        //             functionToolbarButton.IsEnabled = enable;
        //         }

        //         //menu item
        //         if (functionMenu != null)
        //         {
        //             functionMenu.IsEnabled = enable;
        //         }

        //         //stand-alone cancel button
        //         if (cancelButton != null)
        //         {
        //             cancelButton.IsEnabled = !enable;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Invoke any delegate that has been registered
        ///  to cancel a long-running background process.
        /// </summary>
        // private void InvokeActionCancel()
        // {
        //     try
        //     {
		// 		//execute cancellation hook
		// 		cancelDelegate?.Invoke();
		// 	}
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Load from app config; override with command line if present
        /// </summary>
        /// <returns>bool</returns>
        private static bool LoadParameters()
        {
            bool returnValue = default;
			try
			{
                // First, get configured values

                //get filename from App.config
                if (!Configuration.ReadString("SettingsFilename", out string _settingsFilename))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsFilename: {0}", "SettingsFilename"));
                }
                if ((_settingsFilename == null) || (_settingsFilename == SettingsController<MVCSettings>.FILE_NEW))
                {
                    throw new ApplicationException(string.Format("Settings filename not set: '{0}'.\nCheck SettingsFilename in App.config file.", _settingsFilename));
                }
                //use with the supplied path
                SettingsController<MVCSettings>.Filename = _settingsFilename;

                //identify directory to specified, or use default
                if (Path.GetDirectoryName(_settingsFilename)?.Length == 0)
                {
                    //supply default path if missing
                    SettingsController<MVCSettings>.Pathname = Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator();
                }

                //get serialization format from App.config
                if (!Configuration.ReadString("SettingsSerializeAs", out string _settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsSerializeAs: {0}", "SettingsSerializeAs"));
                }
                if (string.IsNullOrEmpty(_settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Settings serialization format not set: '{0}'.\nCheck SettingsSerializeAs in App.config file.", _settingsSerializeAs));
                }
                //use with the filename
                SettingsBase.SerializeAs = SettingsBase.ToSerializationFormat(_settingsSerializeAs);

                //Second, override with passed values

                if ((Program.Filename != default) && (Program.Filename != SettingsController<MVCSettings>.FILE_NEW))
                {
                    //got filename from command line
                    SettingsController<MVCSettings>.Filename = Program.Filename;
                }
                if (Program.Directory != default)
                {
                    //get default directory from command line
                    SettingsController<MVCSettings>.Pathname = Program.Directory.WithTrailingSeparator();
                }
                if (Program.Format != default)
                {
                    // get default format from command line
                    SettingsBase.SerializeAs = Program.Format switch
                    {
                        "xml" => SettingsBase.SerializationFormat.Xml,
                        "json" => SettingsBase.SerializationFormat.Json,
                        _ => default
                    };
                }

				returnValue = true;
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
				//throw;
			}
			return returnValue;
        }

        private void BindSizeAndLocation()
        {
            //Note:Size must be done after InitializeComponent(); do Location this way as well.--SJS
            // this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::MVCForms.Properties.Settings.Default, "Location", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // this.DataBindings.Add(new System.Windows.Forms.Binding("ClientSize", global::MVCForms.Properties.Settings.Default, "Size", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            Height = Properties.Settings.Default.Size.Height;
            Width = Properties.Settings.Default.Size.Width;
            //TODO:this.Position.X =  global::MvcForms.Core.AvaloniaUI.Properties.Settings.Default.Location.X;
            //TODO:this.Position.Y =  global::MvcForms.Core.AvaloniaUI.Properties.Settings.Default.Location.Y;
        }
        #endregion Utility

        #endregion Methods

        #region Actions
        // private async Task DoSomething()
        // {
		// 	await Task.Delay(3000);
		// }

        #endregion Actions

    }
}
