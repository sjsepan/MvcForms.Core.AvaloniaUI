using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;

namespace MvcForms.Core.AvaloniaUI
{
    public partial class App : Application //TODO:suppress RCS1043 false positive
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                desktop.MainWindow = new MvcView();
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}