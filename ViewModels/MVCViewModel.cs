﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using Ssepan.Utility.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.AvaloniaUI;
using MvcLibrary.Core;
using Avalonia.Controls;
using Avalonia.Media.Imaging;

namespace MvcForms.Core.AvaloniaUI
{
	/// <summary>
	/// Note: this class can subclass the base without type parameters.
	/// </summary>
	public class MVCViewModel :
        FormsViewModel<Bitmap, MVCSettings, MVCModel, MvcView>
    {
        #region Constructors
        public MVCViewModel() { }//Note: not called, but need to be present to compile--SJS

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, Bitmap> actionIconImages,
            FileDialogInfo<Window, MessageBox.MessageBoxResult> settingsFileDialogInfo
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, Bitmap> actionIconImages,
            FileDialogInfo<Window, MessageBox.MessageBoxResult> settingsFileDialogInfo,
            MvcView view
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo, view)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// model specific, not generic
        /// </summary>
        internal void DoSomething()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Doing something...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                ModelController<MVCModel>.Model.SomeBoolean = !ModelController<MVCModel>.Model.SomeBoolean;
				ModelController<MVCModel>.Model.SomeInt++;
                ModelController<MVCModel>.Model.SomeString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = !ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
				ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt++;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = !ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
				ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt++;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = DateTime.Now.ToString();
                //TODO:implement custom messages
                UpdateStatusBarMessages(null, null, DateTime.Now.ToLongTimeString());

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar("Did something.");
            }
        }

        // public override void WindowNewWindow()
        // {
        //     // string errorMessage = null;
        //     StatusMessage = string.Empty;
        //     ErrorMessage = string.Empty;

        //     try
        //     {
        //         StartProgressBar
        //         (
        //             "New Window" + ACTION_IN_PROGRESS,
        //             null,
        //             null, //_actionIconImages["Xxx"],
        //             true,
        //             33
        //         );

        //         if (true)
        //         {
        //             StatusMessage += ACTION_DONE;
        //         }
        //         // else
        //         // {
        //         //     ErrorMessage = errorMessage;
        //         // }

        //         //ModelController<MVCModel>.Model.Refresh();
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //         StopProgressBar(null, string.Format("{0}", ex.Message));
        //     }
        //     finally
        //     {
        //         StopProgressBar(StatusMessage);
        //     }
        // }

        // public override void WindowTile()
        // {
        //     // string errorMessage = null;
        //     StatusMessage = string.Empty;
        //     ErrorMessage = string.Empty;

        //     try
        //     {
        //         StartProgressBar
        //         (
        //             "Tile" + ACTION_IN_PROGRESS,
        //             null,
        //             null, //_actionIconImages["Xxx"],
        //             true,
        //             33
        //         );

        //         if (true)
        //         {
        //             StatusMessage += ACTION_DONE;
        //         }
        //         // else
        //         // {
        //         //     ErrorMessage = errorMessage;
        //         // }

        //         //ModelController<MVCModel>.Model.Refresh();
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //         StopProgressBar(null, string.Format("{0}", ex.Message));
        //     }
        //     finally
        //     {
        //         StopProgressBar(StatusMessage);
        //     }
        // }

        // public override void WindowCascade()
        // {
        //     // string errorMessage = null;
        //     StatusMessage = string.Empty;
        //     ErrorMessage = string.Empty;

        //     try
        //     {
        //         StartProgressBar
        //         (
        //             "Cascade" + ACTION_IN_PROGRESS,
        //             null,
        //             null, //_actionIconImages["Xxx"],
        //             true,
        //             33
        //         );

        //         if (true)
        //         {
        //             StatusMessage += ACTION_DONE;
        //         }
        //         // else
        //         // {
        //         //     ErrorMessage = errorMessage;
        //         // }

        //         //ModelController<MVCModel>.Model.Refresh();
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //         StopProgressBar(null, string.Format("{0}", ex.Message));
        //     }
        //     finally
        //     {
        //         StopProgressBar(StatusMessage);
        //     }
        // }

        // public override void WindowArrangeAll()
        // {
        //     // string errorMessage = null;
        //     StatusMessage = string.Empty;
        //     ErrorMessage = string.Empty;

        //     try
        //     {
        //         StartProgressBar
        //         (
        //             "Arrange All" + ACTION_IN_PROGRESS,
        //             null,
        //             null, //_actionIconImages["Xxx"],
        //             true,
        //             33
        //         );

        //         if (true)
        //         {
        //             StatusMessage += ACTION_DONE;
        //         }
        //         // else
        //         // {
        //         //     ErrorMessage = errorMessage;
        //         // }

        //         //ModelController<MVCModel>.Model.Refresh();
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //         StopProgressBar(null, string.Format("{0}", ex.Message));
        //     }
        //     finally
        //     {
        //         StopProgressBar(StatusMessage);
        //     }
        // }

        // public override void WindowHide()
        // {
        //     // string errorMessage = null;
        //     StatusMessage = string.Empty;
        //     ErrorMessage = string.Empty;

        //     try
        //     {
        //         StartProgressBar
        //         (
        //             "Hide" + ACTION_IN_PROGRESS,
        //             null,
        //             null, //_actionIconImages["Xxx"],
        //             true,
        //             33
        //         );

        //         if (true)
        //         {
        //             StatusMessage += ACTION_DONE;
        //         }
        //         // else
        //         // {
        //         //     ErrorMessage = errorMessage;
        //         // }

        //         //ModelController<MVCModel>.Model.Refresh();
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //         StopProgressBar(null, string.Format("{0}", ex.Message));
        //     }
        //     finally
        //     {
        //         StopProgressBar(StatusMessage);
        //     }
        // }

        // public override void WindowShow()
        // {
        //     // string errorMessage = null;
        //     StatusMessage = string.Empty;
        //     ErrorMessage = string.Empty;

        //     try
        //     {
        //         StartProgressBar
        //         (
        //             "Show" + ACTION_IN_PROGRESS,
        //             null,
        //             null, //_actionIconImages["Xxx"],
        //             true,
        //             33
        //         );

        //         if (true)
        //         {
        //             StatusMessage += ACTION_DONE;
        //         }
        //         // else
        //         // {
        //         //     ErrorMessage = errorMessage;
        //         // }

        //         //ModelController<MVCModel>.Model.Refresh();
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //         StopProgressBar(null, string.Format("{0}", ex.Message));
        //     }
        //     finally
        //     {
        //         StopProgressBar(StatusMessage);
        //     }
        // }

        /// <summary>
        /// get font name
        /// </summary>
        public void GetFont()
        {
            string errorMessage = null;
            FontDialogInfo<Window, string, string> fontDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Get Font...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

				//Note:must be set or gets null ref error after dialog closes
				const string fontDescription = null;
                fontDialogInfo = new FontDialogInfo<Window, string, string>
                (
                    View,
                    true,
                    "Select Font",
                    null,
                    fontDescription
                );

                if (Dialogs.GetFont(ref fontDialogInfo, ref errorMessage))
                {
                    if (fontDialogInfo.Response == "Ok")
                    {
                        StatusMessage += fontDialogInfo.FontDescription;
                    }
                    else
                    {
                        StatusMessage += "cancelled";
                    }
                }
                else
                {
                    ErrorMessage = errorMessage;
                }

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar(StatusMessage);
            }
        }

        /// <summary>
        /// get color RGB
        /// </summary>
        public void GetColor()
        {
            string errorMessage = null;
            ColorDialogInfo<Window, string, string> colorDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Get Color...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

				const string color = null;
                colorDialogInfo = new ColorDialogInfo<Window, string, string>
                (
                    View,
                    true,
                    "Select Color",
                    null,
                    color
                );

                if (Dialogs.GetColor(ref colorDialogInfo, ref errorMessage))
                {
                    if (colorDialogInfo.Response == "Ok")
                    {
                        StatusMessage +=
                            string.Format
                            (
                                "Red/Green/Blue:{0}",
                                colorDialogInfo.Color
                            );
                    }
                    else
                    {
                        StatusMessage += "cancelled";
                    }
                }
                else
                {
                    ErrorMessage = errorMessage;
                }

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar(StatusMessage);
            }
        }
        #endregion Methods

    }
}
