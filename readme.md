# readme.md - README for MvcForms.Core.AvaloniaUI

## Desktop GUI app demo, on Linux, in C# / DotNet[8] / AvaloniaUI, using VSCode

### Purpose

To illustrate a working Desktop GUI app in C# on .Net (Core). Uses AvaloniaUI where WinForms was being used.

![MvcForms.Core.AvaloniaUI.png](./MvcForms.Core.AvaloniaUI.png?raw=true "Screenshot")

### Usage notes

~This application uses keys in app.config to identify a settings file to automatically load, for manual processing and saving. 1) The file name is given, and the format given must be one of two that the app library Ssepan.Application.Core knows how to serialize / deserialize: json, xml. 2) The value used must correspond to the format expected by the SettingsController in Ssepan.Application.Core. It is specified with the SerializeAs property of type SerializationFormat enum. 3) The file must reside in the user's personal directory, which on Linux is /home/username. A sample file can be found in the MvcLibrary.Core project in the Sample folder.
~The automatic loading is not necessary (it is a carry-over from the original console app) and can be disabled by editing the code in InitViewModel() in MvcView.axaml.cs to remove the ViewModel.FileOpen() call.
~Dialogs in AvaloniaUI are designed to be used async. Otherwise, they tend to hang on display, and you have to Ctrl-C at the terminal to stop the app.
~TextBox.Text uses an inherited event TextInput that does not work the same and is harder to wire-up in the first place. To wire up the event (see <https://github.com/AvaloniaUI/Avalonia/issues/3491>) you must call .AddHandler in code, as the TextInput attribute in axaml seems to have not effect (even though the compiler expected it to point to an appropriate handler.) Even once it is wired correctly, the behavior is not as typically expected/desired: when the handler runs, the TextBox .Text contains original value, not the new one that is visible, and the event argument e.Text contains just the characters typed, not the new value. Instead use an alternate observable subscription (see <https://github.com/AvaloniaUI/Avalonia/issues/418>) to obtain the new text value.

### Instructions for downloading/installing .Net 'Core'

<https://dotnet.microsoft.com/download>
<https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website>

### MessageBox

dotnet add package MessageBox.Avalonia
<https://github.com/AvaloniaCommunity/MessageBox.Avalonia>

### Install package for app configuration

~in folder for project use:
dotnet add package System.Configuration.ConfigurationManager

### Debugging

"Avalonia has an inbuilt DevTools window which is enabled by calling the attached AttachDevTools() method in a Window constructor. The default templates have this enabled when the program is compiled in DEBUG mode:
To open the DevTools, press F12, or pass a different Gesture to the this.AttachDevTools() method."
<https://docs.avaloniaui.net/docs/getting-started/developer-tools>

### Avalonia reference

<https://docs.avaloniaui.net/>

### Instructions/example of adding Avalonia to dotnet and creating app ui in VSCode

<https://dev.to/carlos487/avalonia-ui-in-ubuntu-getting-started-2fak>

### HelloWorld tutorial for AvaloniaUI/C# on Linux, w/ instructions on adding Avalonia templates to .Net

<https://www.nequalsonelifestyle.com/2019/05/08/avalonia-hello-world/>

### Multiplatform UI Coding with AvaloniaUI in Easy Samples

<https://www.codeproject.com/Articles/5308645/Multiplatform-UI-Coding-with-AvaloniaUI-in-Easy-Sa>

### Q&A

<https://github.com/AvaloniaUI/Avalonia/discussions>

### Enhancements

1.2:
~test app w/ add'l themes: Simple, Classic (Classic.Avalonia.Theme by @bandysc); set current to Classic

1.1:
~replace obsolete dialog calls with calls to IStorageProvider dialogs

1.0:
~update to net8.0
~refactor to newer C# features
~refactor to existing language types
~perform format linting
~redo command-line args and how passed filename overrides config file settings, including addition of a format arg.

0.10:
~switch from MessageBox.Avalonia to custom version derived from MessageBox by kekekeks

0.9:
~add more std menus
~remove ACTION_ constants moved into VM base
~fix quit trapping

0.8:
~Initial release.

## Contact

Steve Sepan
<sjsepan@yahoo.com>
12/11/2024
